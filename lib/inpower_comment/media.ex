defmodule InpowerComment.Media do
  use Ecto.Schema
  import Ecto.Changeset

  alias InpowerComment.Comments
  # alias InpowerComment.Replies
  @primary_key {:id, :binary_id, autogenerate: true}


  schema "comment_medias" do

    field :media_url, :string
    field :status, :string

    belongs_to(:comment, Comments, type: :binary_id)
    # belongs_to(:reply, Replies, type: :binary_id)
    timestamps()
  end

  @doc false
  def changeset(media, attrs) do
    # IO.inspect media
    # IO.inspect attrs
    media
    |> cast(attrs, [:status, :media_url, :comment_id, ]) #:reply_id
    |> validate_required([:status, :media_url])
    |> foreign_key_constraint(:comment_id)
    # |> foreign_key_constraint(:reply_id)
  end
end
