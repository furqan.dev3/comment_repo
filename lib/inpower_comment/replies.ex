defmodule InpowerComment.Replies do
  use Ecto.Schema
  import Ecto.Changeset
  alias InpowerComment.Reply_medias
  @primary_key {:id, :binary_id, autogenerate: true}

  schema "reply" do
    field :commentid, :string
    field :isdeletedbyadmin, :boolean, default: false
    field :likecount, :integer
    field :postid, :string
    field :reply, :string
    # field :replyid, Ecto.UUID, autogenerate: true
    field :status, :integer
    field :userid, :string
    field :userlikes, :integer

    timestamps()

    has_many(:reply_media, Reply_medias, foreign_key: :reply_id)

  end

  @doc false
  def changeset(replies, attrs) do
    replies
    |> cast(attrs, [:reply, :isdeletedbyadmin, :userid, :postid, :status, :likecount, :userlikes, :commentid])
    |> validate_required([:reply, :isdeletedbyadmin, :userid, :postid, :status, :likecount, :userlikes, :commentid])
  end
end
