defmodule InpowerComment.Reply_medias do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}

  schema "reply_media" do
    field :media_url, :string
    field :status, :string
    belongs_to(:reply, Replies, type: :binary_id)
    timestamps()
  end

  @doc false
  def changeset(reply_medias, attrs) do
    reply_medias
    |> cast(attrs, [:media_url, :status, :reply_id])
    |> validate_required([:media_url, :status])
    |> foreign_key_constraint(:reply_id)
  end
end
