defmodule InpowerComment.HandleDb do
  import Ecto.Query, warn: false
  alias InpowerComment.Repo
  alias InpowerComment.Comment
  alias InpowerComment.Comments
  alias InpowerComment.Replies
  alias InpowerComment.Reply
  alias InpowerComment.Media_db
  alias InpowerComment.Reply_medias
  alias InpowerComment.Reply_medias_db
  alias InpowerComment.Media
  #new changing

  def create_comments_in_Table(comment, isdeletedbyadmin, userid,  postid, status, userlikes, likecount, media_url) do


    # IO.inspect reply_id
    {:ok, comment} = InpowerComment.Comment.create_comments( %{
      comments: comment,
      isdeletedbyadmin: isdeletedbyadmin,
      userid: userid,
      postid: postid,
      status: status,
      likecount: likecount,
      userlikes: userlikes,
     })

    case media_url do
    " " ->
      {:ok , "Empty url"}
    _ ->

     case InpowerS3.base64_to_upload(media_url, "comments/replies") do
      {:ok, media_url} ->
        InpowerComment.Media_db.create_media( %{

          comment_id: comment.id,
          status: "True",
          media_url: media_url
       })
      {:error, _} ->
        {:error, "Failed to upload Media file"}
      end
    end
  end

  def get_posts_comment() do
    InpowerComment.Comment.list_comments()
  end

  def get_comment_by_id(postid) do
    InpowerComment.Comment.get_comments!(postid)
  end

  def update_comments_in_table(comment, isdeletedbyadmin, userid, postid, status, userlikes, likecount, commentid,  media_url) do

    query = from c in Comments,
            where:  c.id == ^commentid


    change_data = Repo.all(query) |> Enum.at(0)
    {:ok, comment} = InpowerComment.Comment.updating_comments(change_data,
      %{
        comments: comment,
        id: commentid,
        isdeletedbyadmin: isdeletedbyadmin,
        userid: userid,
        postid: postid,
        status: status,
        likecount: likecount,
        userlikes: userlikes,
      })
      case media_url do
        " " ->
          IO.inspect(media_url, label: "here is media ")
          {:ok , "Empty url"}
        _ ->
          IO.inspect(media_url, label: "here is media _ ")
        case InpowerS3.base64_to_upload(media_url, "comments/replies") do
          {:ok, media_url} ->
            IO.inspect(media_url, label: "here is media 2")
            query = from c in Media,
              where:  c.comment_id == ^commentid


            change_data_media = Repo.all(query) |> Enum.at(0)
            # IO.inspect change_data_media
            InpowerComment.Media_db.updating_media(change_data_media, %{
              comment_id: comment.id,
              status: "True",
              media_url: media_url
          })
          {:error, _} ->
            IO.inspect(media_url, label: "here is media e")
            {:error, "Failed to upload Media file"}
        end
      end
  end


  def delete_comment_by_id(commentid) do
    InpowerComment.Comment.delete_comments_in_repo(commentid)
  end

  def delete_reply_by_id(id) do
    InpowerComment.Reply.delete_replyid_in_repo(id)

  end

  def get_replies_by_id(commentid) do
    InpowerComment.Reply.get_reply!(commentid)
  end

  def create_replies_in_Table(isdeletedbyadmin, userid,  reply, postid, status, userlikes, likecount, commentid, media_url) do

    # IO.puts "here is media"
    # IO.inspect(commentid)
    query = from c in Comments,
            where:  c.id == ^commentid
    get_comment_for_reply = Repo.all(query) |> Enum.at(0)
    # IO.puts "here is media 1"
    # IO.inspect get_comment_for_reply
    case get_comment_for_reply do
      nil ->
        {:error, "false"}
      _ ->
        # IO.puts "here is media 2"
        # IO.inspect media_url
        {:ok, replys} = InpowerComment.Reply.create_Replies( %{
          isdeletedbyadmin: isdeletedbyadmin,
          userid: userid,
          reply: reply,
          postid: postid,
          status: status,
          likecount: likecount,
          userlikes: userlikes,
          commentid: commentid
         })
          # IO.inspect "here after insert reply"
         case media_url do
          " " ->
            {:ok , "Empty url"}
          _ ->

           case InpowerS3.base64_to_upload(media_url, "comments/replies") do
            {:ok, media_url} ->
              # IO.puts "here is media 3"
              # IO.inspect replys.id
              InpowerComment.Reply_medias_db.create_media( %{
                comment_id: commentid,
                reply_id: replys.id,
                status: "True",
                media_url: media_url
             })
            {:error} ->
              {:error, "Failed to upload Media file"}
            end
          end
    end
  end

  def update_reply_in_table(reply, isdeletedbyadmin, userid,  replyid, postid, status, userlikes, likecount, commentid , media_url) do

    query = from c in Replies,
            where:  c.id == ^replyid

    change_data = Repo.all(query) |> Enum.at(0)
    # IO.inspect change_data
    {:ok, reply} = InpowerComment.Reply.updating_Replies(change_data,
      %{
        reply: reply,
        isdeletedbyadmin: isdeletedbyadmin,
        userid: userid,
        id: replyid,
        postid: postid,
        status: status,
        likecount: likecount,
        userlikes: userlikes,
      })
      case media_url do
        " " ->
          query = from m in Reply_medias,
              where:  m.reply_id == ^replyid


            change_data_media = Repo.all(query) |> Enum.at(0)
            IO.inspect change_data_media
            InpowerComment.Reply_medias_db.updating_media(change_data_media, %{
              reply_id: reply.id,
              status: "False",

          })
        _ ->
        case InpowerS3.base64_to_upload(media_url, "comments/replies") do
          {:ok, media_url} ->
            query = from m in Reply_medias,
              where:  m.reply_id == ^replyid


            change_data_media = Repo.all(query) |> Enum.at(0)
            # IO.inspect change_data_media
            InpowerComment.Reply_medias_db.updating_media(change_data_media, %{
              reply_id: reply.id,
              status: "True",
              media_url: media_url
          })
          {:error, _} ->
            {:error, "Failed to upload Media file"}
        end
      end

  end
end
