defmodule InpowerComment.Reply_medias_db do

  import Ecto.Query, warn: false
  alias InpowerComment.Repo
  alias InpowerComment.Reply_medias
  alias InpowerComment.Replies
  alias InpowerComment.Comments


  def list_media do
    Repo.all(Reply_medias)
  end

  def get_media!(replyid) do
    query =
      from(c in Reply_medias,
        where: c.replyid == ^replyid
      )

    {:ok, Repo.all(query)}

  end

  def create_media(attrs \\ %{}) do
    IO.inspect attrs
    %Reply_medias{}
    |> Reply_medias.changeset(attrs)
    |> Repo.insert()
    |> IO.inspect
  end


  def updating_media(%Reply_medias{} = medias, attrs) do
    medias
    |> Reply_medias.changeset(attrs)
    |> Repo.update()
  end


  def delete_media(%Reply_medias{} = medias) do
    Repo.delete(medias)
  end



  def delete_media_in_repo(replyid) do
    query =
          from(c in Reply_medias,
            where:  c.replyid == ^replyid
            )


    Repo.delete_all(query)
  end


  def change_media(%Reply_medias{} = medias, attrs \\ %{}) do
    Reply_medias.changeset(medias, attrs)
  end
end
