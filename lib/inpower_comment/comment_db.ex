defmodule InpowerComment.Comment do

  import Ecto.Query, warn: false
  alias InpowerComment.Repo
  alias InpowerComment.Media
  alias InpowerComment.Replies
  alias InpowerComment.Comments


  def list_comments do
    Repo.all(Comments)
  end

  def get_comments!(postid) do


############### working query but faild to encode in rGPC server ######################3

    # data = Repo.all(from c in Comments, where:  c.postid == ^postid) |> Repo.preload(:medias)
    # IO.inspect data

    # {:ok, data}

###################3######################################################################

################################ example from IMO other dev ##############################
    # set =
    #   from(f in Follower,
    #     where: f.follow_id == ^id,
    #     select: f.profile_id
    #   )

    # Profile
    # |> where([p], p.id in subquery(set))
    # |> join(:left, [p], impacting_count in assoc(p, :impacting))
    # |> join(:left, [p, _], inspired_count in assoc(p, :inspired))
    # |> join(:left, [p, _], country in assoc(p, :country))
    # |> group_by([p, , , country], [p.id, country.id, country.name])
    # |> select(
    #   [p, impacting_count, inspired_count, country],
    #   %{
    #     id: p.id,
    #     fullname: p.fullname,
    #     email: p.email,
    #     phone: p.phone,
    #     invite_code: p.invite_code,
    #     type: p.type,
    #     username: p.username,
    #     bio: p.bio,
    #     visibility: p.visibility,
    #     profile_photo: p.profile_photo,
    #     auth_id: p.auth_id,
    #     inspired_count: count(inspired_count.id),
    #     impacting_count: count(impacting_count.id),
    #     country: %{
    #       id: country.id,
    #       name: country.name
    #     }
    #   }
    # )
    # |> Repo.all()


################################################################################


####################### try with joinning table ########################################

    comments = Comments
    |> where([comments], comments.postid == ^postid)
    |> join( :left, [comments], medias in assoc(comments, :medias))
    |> select(
      [comments, medias],
      %{
        comments: comments.comments,
        id: comments.id,
        isdeletedbyadmin: comments.isdeletedbyadmin,
        likecount: comments.likecount,
        postid: comments.postid,
        status: comments.status,
        userid: comments.userid,
        userlikes: comments.userlikes,
        medias: %{
          id: medias.id,
          status: medias.status,
          comment_id: medias.comment_id,
          media_url: medias.media_url
        }
      }
    )

    |> Repo.all()



    {:ok, comments}
########################################################################################

    # query1 =
    #   from(c in Comments,
    #     where: c.postid == ^postid,
    #     # perload: [:medias]
    #     left_join: media in assoc(c :medias),
    #     perload: [:medias]
        # left_join: m in Media, as: :media, on: m.commentid == c.commentid
      #   include: [
      #     {
      #     model: Media,
      #     as: 'media',
      #   }
      # ]
      # )
    #   IO.inspect query1, label: "query1----before repo all"


    # data =
    # query1
    # |> Repo.all()
    # # |> IO.inspect
    # |> Repo.preload(:medias)
    # # |> IO.inspect

    # # data =
    # # data
    # # |> preload(:medias)
    # IO.inspect data
    # {:ok, comments}

  end

  def create_comments(attrs \\ %{}) do
    %Comments{}
    |> Comments.changeset(attrs)
    |> Repo.insert()
  end


  def updating_comments(%Comments{} = comments, attrs) do
    comments
    |> Comments.changeset(attrs)
    |> Repo.update()
  end


  def delete_comments(%Comments{} = comments) do
    Repo.delete(comments)
  end



  def delete_comments_in_repo(commentid) do
    query =
          from(c in Comments,
            where:  c.id == ^commentid
            )
    # delete_media_by_commendid_in_repo(commentid)
    delete_reply_by_commentid_in_repo(commentid)
    Repo.delete_all(query)
  end

  defp delete_media_by_commendid_in_repo(commentid) do
    query =
    from(c in Media,
    where:  c.comment_id == ^commentid
    )

    Repo.delete_all(query)
  end

  defp delete_reply_by_commentid_in_repo(commentid) do
    query =
            from(c in Replies,
            where:  c.commentid == ^commentid
            )

    Repo.delete_all(query)

  end

  def change_comments(%Comments{} = comments, attrs \\ %{}) do
    Comments.changeset(comments, attrs)
  end
end
