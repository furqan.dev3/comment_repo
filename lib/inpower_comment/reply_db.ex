defmodule InpowerComment.Reply do

  import Ecto.Query, warn: false
  alias InpowerComment.Repo
  alias InpowerComment.Media
  alias InpowerComment.Reply_medias_db
  alias InpowerComment.Replies
  alias InpowerComment.Comments


  def list_Replies do
    # IO.inspect
    Repo.all(Replies)
  end

  def get_reply!(commentid) do

    # query = from(c in Replies,
            # where:  c.commentid == ^commentid)

    reply = Replies
    |> where([reply], reply.commentid == ^commentid)
    |> join( :left, [reply], reply_media in assoc(reply, :reply_media))
    |> select(
      [reply, reply_media],
      %{
        reply: reply.reply,
        id: reply.id,
        isdeletedbyadmin: reply.isdeletedbyadmin,
        likecount: reply.likecount,
        postid: reply.postid,
        status: reply.status,
        userid: reply.userid,
        userlikes: reply.userlikes,
        medias: %{
          id: reply_media.id,
          status: reply_media.status,
          reply_id: reply_media.reply_id,
          media_url: reply_media.media_url
        }
      }
    )
    |> Repo.all()



    {:ok, reply}
  end

  def create_Replies(attrs \\ %{}) do
    # IO.inspect attrs
    %Replies{}
    |> Replies.changeset(attrs)
    |> Repo.insert()
    # |> IO.inspect
  end


  def updating_Replies(%Replies{} = replies, attrs) do
    IO.inspect attrs
    replies
    |> Replies.changeset(attrs)
    |> Repo.update()
    |> IO.inspect
  end


  def delete_Replies(%Replies{} = replies) do
    Repo.delete(replies)
  end



  def delete_replyid_in_repo(id) do
    query = from c in Replies,
            where:  c.id == ^id

    Repo.delete_all(query)

  end



  def change_Replies(%Replies{} = replies, attrs \\ %{}) do
    Replies.changeset(replies, attrs)
  end
end
