defmodule InpowerComment.Comments do
  use Ecto.Schema
  import Ecto.Changeset

  alias InpowerComment.Media
  @primary_key {:id, :binary_id, autogenerate: true}

  schema "comment" do
    # field :id, Ecto.UUID, autogenerate: true
    field :comments, :string
    field :isdeletedbyadmin, :boolean, default: false
    field :likecount, :integer
    field :postid, :string
    field :status, :integer
    field :userid, :string
    field :userlikes, :integer

    timestamps()

    has_many(:medias, Media, foreign_key: :comment_id)

  end

  @doc false
  def changeset(comments, attrs) do
    comments
    |> cast(attrs, [:comments, :isdeletedbyadmin, :userid, :postid, :status, :likecount, :userlikes])
    |> validate_required([:comments, :isdeletedbyadmin, :userid, :postid, :status, :likecount, :userlikes])
  end
end
