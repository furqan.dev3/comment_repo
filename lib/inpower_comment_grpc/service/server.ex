defmodule InpowerCommentGrpc.Service.Server do
  use GRPC.Server, service: Commentapi.CommentService.Service

  alias Commentapi.{
    CreateCommentRequest,
    CreateCommentResponse,

    GetCommentRequest,
    GetCommentResponse,

    DeleteCommentRequest,
    DeleteCommentResponse,

    UpdateCommentRequest,
    UpdateCommentResponse,

    CreateReplyRequest,
    CreateReplyResponse,

    GetReplyRequest,
    GetReplyResponse,

    DeleteReplyRequest,
    DeleteReplyResponse,

    UpdateReplyRequest,
    UpdateReplyResponse

  }

  alias InpowerComment.HandleDb

  require Logger

  ####################### Comments #####################################################

  def get_comment(%GetCommentRequest{postid: postid}, _stream) do
    with {:ok, getting_comment} <- InpowerComment.HandleDb.get_comment_by_id(postid) do
      # comment =
      # getting_comment
      # |> Enum.map(fn(x) -> Map.from_struct(x) end)
      # |> IO.inspect
      # IO.inspect getting_comment
      # IO.inspect getting_comment, label: "first perload"
      GetCommentResponse.new(comments: getting_comment)

    else
      _error ->
        Logger.info("Did not find arguments #{postid}")
        GetCommentResponse.new()
    end
  end

  def create_comment(%CreateCommentRequest{comment: comment, isdeletedbyadmin: isdeletedbyadmin, userid: userid,  postid: postid, status: status, likecount: likecount, userlikes: userlikes, media_url: media_url}, _stream) do
    # IO.inspect InpowerComment.HandleDb.create_comments_in_Table(comment, isdeletedbyadmin, userid,  replyid, postid, status, userlikes, likecount, commentid, media_url)
    with {:ok, comments} <- InpowerComment.HandleDb.create_comments_in_Table(comment, isdeletedbyadmin, userid,   postid, status, userlikes, likecount, media_url) do
      CreateCommentResponse.new(comments: comments) |> IO.inspect
    else
      _error ->
        Logger.info("Did not find arguments #{postid}")
        CreateCommentResponse.new()
    end
  end

  def update_comment(%UpdateCommentRequest{comment: comment, isdeletedbyadmin: isdeletedbyadmin, userid: userid, postid: postid, status: status, likecount: likecount, userlikes: userlikes, id: commentid, media_url: media_url}, _stream) do
    IO.inspect commentid, label: "from server"
    with {:ok, updating_comment} <- InpowerComment.HandleDb.update_comments_in_table(comment, isdeletedbyadmin, userid, postid, status, userlikes, likecount, commentid, media_url) do
      UpdateCommentResponse.new(updating_comment: updating_comment)
    else
      _error ->
        Logger.info("Did not find arguments #{commentid}")
        UpdateCommentResponse.new()
    end
  end

  def delete_comment(%DeleteCommentRequest{id: commentid}, _stream) do
    IO.inspect commentid, label: "from server"
    with {1, deleting_comment} <- InpowerComment.HandleDb.delete_comment_by_id(commentid) do
      case deleting_comment do
        nil ->
          DeleteCommentResponse.new(status: true)
        _ ->
          DeleteCommentResponse.new(status: false)
      end

    else
      _error ->
        Logger.info("Did not find arguments #{commentid}")
        DeleteCommentResponse.new()
    end
  end

  ################################# Replies ###########################################

  def get_reply(%GetReplyRequest{commentid: commentid}, _stream) do
    with {:ok, getting_reply} <- InpowerComment.HandleDb.get_replies_by_id(commentid) do

      # replies =
      # getting_reply
      # |> Enum.map(fn(x) -> Map.from_struct(x) end)
      IO.inspect getting_reply, label: "first perload"
      GetReplyResponse.new(replies: getting_reply) |> IO.inspect
    else
      _error ->
        Logger.info("Did not find arguments #{commentid}")
        GetReplyResponse.new()
    end
  end

  def create_reply(%CreateReplyRequest{reply: reply, isdeletedbyadmin: isdeletedbyadmin, userid: userid, postid: postid, status: status, likecount: likecount, userlikes: userlikes, commentid: commentid, media_url: media_url}, _stream) do
    with {:ok, creating_reply} <- InpowerComment.HandleDb.create_replies_in_Table(isdeletedbyadmin, userid,  reply, postid, status, userlikes, likecount, commentid, media_url) do
      CreateReplyResponse.new(creating_reply: creating_reply)
    else
      _error ->
        Logger.info("Did not find arguments #{commentid}")
        CreateReplyResponse.new()
    end
  end

  def update_reply(%UpdateReplyRequest{reply: reply, isdeletedbyadmin: isdeletedbyadmin, userid: userid, id: id, postid: postid, status: status, likecount: likecount, userlikes: userlikes, commentid: commentid, media_url: media_url}, _stream) do
    with {:ok, updating_reply} <- InpowerComment.HandleDb.update_reply_in_table(reply, isdeletedbyadmin, userid,  id, postid, status, userlikes, likecount, commentid, media_url) do
      UpdateReplyResponse.new(updating_reply: updating_reply)
    else
      _error ->
        Logger.info("Did not find arguments #{commentid}")
        UpdateReplyResponse.new()
    end
  end

  def delete_reply(%DeleteReplyRequest{id: id}, _stream) do
    with {1, deleting_reply} <- InpowerComment.HandleDb.delete_reply_by_id(id) do
      case deleting_reply do
      nil ->
        DeleteReplyResponse.new(status: true)
      _ ->
        DeleteReplyResponse.new(status: false)
      end
    else
      _error ->
        Logger.info("Did not find arguments #{id}")
        DeleteReplyResponse.new()
    end
  end
end
