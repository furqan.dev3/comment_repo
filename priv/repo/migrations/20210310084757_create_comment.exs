defmodule InpowerComment.Repo.Migrations.CreateComment do
  use Ecto.Migration

  def change do
    create table(:comment, primary_key: false) do
      add :comments, :string
      add :isdeletedbyadmin, :boolean, default: false, null: false
      add :userid, :string
      add :postid, :string
      add :status, :integer
      add :likecount, :integer
      add :userlikes, :integer
      add :id, :uuid, primary_key: true

      timestamps()
    end
    # create unique_index(:comment, [:id])
  end
end
