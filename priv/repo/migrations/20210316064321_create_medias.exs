defmodule InpowerComment.Repo.Migrations.CreateMedias do
  use Ecto.Migration

  alias InpowerComment.Comments

  def change do
    create table(:comment_medias, primary_key: false) do
      add :comment_id, references(:comment, column: :id, on_delete: :nothing, type: :uuid)
      # add :reply_id, references(:reply, column: :id, on_delete: :nothing, type: :uuid)
      add :status, :string
      add :media_url, :string
      add :id, :uuid, primary_key: true

      timestamps()

    end

  end
end
