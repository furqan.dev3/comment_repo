defmodule InpowerComment.Repo.Migrations.CreateReply do
  use Ecto.Migration

  def change do
    create table(:reply, primary_key: false) do
      add :reply, :string
      add :isdeletedbyadmin, :boolean, default: false, null: false
      add :userid, :string
      add :id, :uuid, primary_key: true
      add :postid, :string
      add :status, :integer
      add :likecount, :integer
      add :userlikes, :integer
      add :commentid, :string

      timestamps()
    end

  end
end
