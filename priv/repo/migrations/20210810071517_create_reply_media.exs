defmodule InpowerComment.Repo.Migrations.CreateReplyMedia do
  use Ecto.Migration

  alias InpowerComment.Replies

  def change do
    create table(:reply_media, primary_key: false) do

      add :reply_id, references(:reply, column: :id, on_delete: :nothing, type: :uuid)
      add :media_url, :string
      add :status, :string

      add :id, :uuid, primary_key: true

      timestamps()
    end

  end
end
